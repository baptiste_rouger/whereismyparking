package org.miage.where_is_my_parking.event;

import org.miage.where_is_my_parking.EntityREST.Capacities.Capacity;

import java.util.List;

/**
 * Created by Rushnak on 10/01/2018.
 */

public class CapacitySearchResultEvent {

    private List<Capacity> capacityList;

    public CapacitySearchResultEvent(List<Capacity> places) {
        this.capacityList = places;
    }

    public List<Capacity> getCapacities() {
        return capacityList;
    }
}
