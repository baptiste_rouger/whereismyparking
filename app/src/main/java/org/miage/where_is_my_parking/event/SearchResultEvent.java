package org.miage.where_is_my_parking.event;

import java.util.List;

import org.miage.where_is_my_parking.EntityREST.Parkings.Parking;

/**
 * Created by alexmorel on 10/01/2018.
 */

public class SearchResultEvent {

    private List<Parking> parkingList;

    public SearchResultEvent(List<Parking> places) {
        this.parkingList = places;
    }

    public List<Parking> getParkings() {
        return parkingList;
    }
}
