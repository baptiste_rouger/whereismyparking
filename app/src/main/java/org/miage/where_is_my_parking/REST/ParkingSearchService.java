package org.miage.where_is_my_parking.REST;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



import java.lang.reflect.Modifier;

import org.miage.where_is_my_parking.EntityREST.Parkings.Parkings;
import org.miage.where_is_my_parking.event.EventBusManager;
import org.miage.where_is_my_parking.event.SearchResultEvent;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by alexmorel on 05/01/2018.
 */

public class ParkingSearchService {

    public static ParkingSearchService INSTANCE = new ParkingSearchService();
    private final ParkingSearchRESTService mParkingSearchRESTService;

    // TO-DO Recherche locale

    private ParkingSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://data.nantes.fr/api/")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        mParkingSearchRESTService = retrofit.create(ParkingSearchRESTService.class);
    }

    public void searchParkings() {
        // Call to the REST service
        mParkingSearchRESTService.searchForParkings().enqueue(new Callback<Parkings>() {
            @Override
            public void onResponse(Call<Parkings> call, retrofit2.Response<Parkings> response) {
                // Post an event so that listening activities can update their UI
                if (response.body() != null && response.body().parkingList != null) {
                    EventBusManager.BUS.post(new SearchResultEvent(response.body().parkingList));
                } else if(response.body() == null ){
                    // Null result
                    // We may want to display a warning to user (e.g. Toast)
                } else {
                }
            }

            @Override
            public void onFailure(Call<Parkings> call, Throwable t) {
                // Request has failed or is not at expected format
                // We may want to display a warning to user (e.g. Toast)
            }
        });
    }

    // Service describing the REST APIs
    public interface ParkingSearchRESTService {

        @GET("publication/24440040400129_NM_NM_00044/LISTE_SERVICES_PKGS_PUB_NM_STBL/content/?format=json/")
        Call<Parkings> searchForParkings();
    }
}
