package org.miage.where_is_my_parking.REST;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.miage.where_is_my_parking.EntityREST.Capacities.OpenData;
import org.miage.where_is_my_parking.EntityREST.Capacities.Response;
import org.miage.where_is_my_parking.event.CapacitySearchResultEvent;
import org.miage.where_is_my_parking.event.EventBusManager;

import java.lang.reflect.Modifier;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by Rushnak on 05/01/2018.
 */

public class CurrentCapacitySearchService {

    public static CurrentCapacitySearchService INSTANCE = new CurrentCapacitySearchService();
    private final CurrentCapacitySearchRESTService mCurrentCapacitySearchRESTService;

    // TO-DO Recherche locale

    private CurrentCapacitySearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://data.nantes.fr/api/")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        mCurrentCapacitySearchRESTService = retrofit.create(CurrentCapacitySearchRESTService.class);
    }

    public void SearchCapacity() {
        // Call to the REST service
        mCurrentCapacitySearchRESTService.searchForCapacity().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                // Post an event so that listening activities can update their UI
                if (response.body() != null && response.body().openData.answer.data.grpParkings.capacities != null) {
                    EventBusManager.BUS.post(new CapacitySearchResultEvent(response.body().openData.answer.data.grpParkings.capacities));
                } else if(response.body() == null ){
                    // Null result
                    // We may want to display a warning to user (e.g. Toast)
                } else {
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                // Request has failed or is not at expected format
                // We may want to display a warning to user (e.g. Toast)
            }
        });
    }

    // Service describing the REST APIs
    public interface CurrentCapacitySearchRESTService {

        @GET("getDisponibiliteParkingsPublics/1.0/U4FD2BMF4MJDXXR/?output=json")
        Call<Response> searchForCapacity();
    }
}
