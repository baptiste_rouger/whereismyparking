package org.miage.where_is_my_parking.RestToBase;

import java.util.ArrayList;
import java.util.List;

import org.miage.where_is_my_parking.Entity.Capacite;
import org.miage.where_is_my_parking.Entity.Contact;
import org.miage.where_is_my_parking.Entity.Localisation;
import org.miage.where_is_my_parking.Entity.Parking;
import org.miage.where_is_my_parking.Entity.dao.DaoParking;

/**
 * Created by Rushnak on 12/03/2018.
 */

public class ParkingTransfo {
    public static ParkingTransfo INSTANCE = new ParkingTransfo();

    public void build(List<org.miage.where_is_my_parking.EntityREST.Parkings.Parking> parkings){
        for (org.miage.where_is_my_parking.EntityREST.Parkings.Parking p : parkings) {
            createParking(p);
        }
    }

    public void update(List<org.miage.where_is_my_parking.EntityREST.Parkings.Parking> parkings) {
        for (org.miage.where_is_my_parking.EntityREST.Parkings.Parking p : parkings) {
            if (DaoParking.loadFromRestId(p.id) == null){
                createParking(p);
            }
        }
    }

    public void createParking (org.miage.where_is_my_parking.EntityREST.Parkings.Parking p){
        p.setDefault();
        Contact contact = new Contact(p.siteWeb, p.exploitant, p.tel);
        Localisation localisation = new Localisation(p.latLong[0], p.latLong[1], p.codePostal, p.commune, p.adresse, p.accesTransCommuns);
        Capacite capacite = new Capacite(p.capaVelo, p.capaVoitures, p.capaVehiElec, p.capaMoto, p.capaPMR);
        boolean CB = false, cash = false, totalGR = false, statVelo = false, statVeloSec = false, accPMR = false;
        if(p.moyenPaiement.contains("CB en borne de sortie"))
            CB = true;
        if(p.moyenPaiement.contains("Espèces"))
            cash = true;
        if(p.moyenPaiement.contains("Total GR"))
            totalGR = true;
        if(p.stationnementVelo.contains("OUI"))
            statVelo = true;
        if(p.stationnementVeloSecurise.contains("OUI"))
            statVeloSec = true;
        if(p.accesPMR.contains("OUI"))
            accPMR = true;
        Parking park = new Parking(p.name.name, p.serviceVelo, p.infoComp, statVelo, statVeloSec, accPMR, CB, cash, totalGR, p.condAcces, p.presentation, p.libCategorie, p.libType, p.id, localisation, capacite, contact);
        contact.save();
        localisation.save();
        capacite.save();
        park.save();
    }
}
