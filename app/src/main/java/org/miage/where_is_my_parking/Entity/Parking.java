package org.miage.where_is_my_parking.Entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Rushnak on 12/03/2018.
 */
@Table(name = "Parking")
public class Parking extends Model {

    @Column(name = "Name")
    private String name;
    @Column(name = "ServiceVelo")
    private String serviceVelo;
    @Column(name = "InfoComp")
    private String infoComp;
    @Column(name = "StationnementVelo")
    private boolean stationnementVelo;
    @Column(name = "StationnementVeloSecurise")
    private boolean stationnementVeloSecurise;
    @Column(name = "AccesPMR")
    private boolean accesPMR;
    @Column(name = "CB")
    private boolean CB;
    @Column(name = "Cash")
    private boolean cash;
    @Column(name = "TotalGR")
    private boolean totalGR;
    @Column(name = "CondAcces")
    private String condAcces;
    @Column(name = "Presentation")
    private String presentation;
    @Column(name = "LibCategorie")
    private String libCategorie;
    @Column(name = "LibType")
    private String libType;
    @Column(name = "IdFromRest")
    private int idFromRest;
    @Column(name = "Localisation")
    private Localisation localisation;
    @Column(name = "Capacite")
    private Capacite capacite;
    @Column(name = "Contact")
    private Contact contact;
    @Column(name = "Favori")
    private boolean favori;

    public Parking(){
        super();
    }

    public Parking(String name, String serviceVelo, String infoComp, boolean stationnementVelo, boolean stationnementVeloSecurise, boolean accesPMR, boolean CB, boolean cash, boolean totalGR, String condAcces, String presentation, String libCategorie, String libType, int id, Localisation localisation, Capacite capacite, Contact contact) {
        super();
        this.name = name;
        this.serviceVelo = serviceVelo;
        this.infoComp = infoComp;
        this.stationnementVelo = stationnementVelo;
        this.stationnementVeloSecurise = stationnementVeloSecurise;
        this.accesPMR = accesPMR;
        this.CB = CB;
        this.cash = cash;
        this.totalGR = totalGR;
        this.condAcces = condAcces;
        this.presentation = presentation;
        this.libCategorie = libCategorie;
        this.libType = libType;
        this.idFromRest = id;
        this.localisation = localisation;
        this.capacite = capacite;
        this.contact = contact;
        favori = false;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceVelo() {
        return serviceVelo;
    }

    public void setServiceVelo(String serviceVelo) {
        this.serviceVelo = serviceVelo;
    }

    public String getInfoComp() {
        return infoComp;
    }

    public void setInfoComp(String infoComp) {
        this.infoComp = infoComp;
    }

    public boolean isStationnementVelo() {
        return stationnementVelo;
    }

    public void setStationnementVelo(boolean stationnementVelo) {
        this.stationnementVelo = stationnementVelo;
    }

    public boolean isStationnementVeloSecurise() {
        return stationnementVeloSecurise;
    }

    public void setStationnementVeloSecurise(boolean stationnementVeloSecurise) {
        this.stationnementVeloSecurise = stationnementVeloSecurise;
    }

    public boolean isAccesPMR() {
        return accesPMR;
    }

    public void setAccesPMR(boolean accesPMR) {
        this.accesPMR = accesPMR;
    }

    public boolean isCB() {
        return CB;
    }

    public void setCB(boolean CB) {
        this.CB = CB;
    }

    public boolean isCash() {
        return cash;
    }

    public void setCash(boolean cash) {
        this.cash = cash;
    }

    public boolean isTotalGR() {
        return totalGR;
    }

    public void setTotalGR(boolean totalGR) {
        this.totalGR = totalGR;
    }

    public String getCondAcces() {
        return condAcces;
    }

    public void setCondAcces(String condAcces) {
        this.condAcces = condAcces;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getLibCategorie() {
        return libCategorie;
    }

    public void setLibCategorie(String libCategorie) {
        this.libCategorie = libCategorie;
    }

    public String getLibType() {
        return libType;
    }

    public void setLibType(String libType) {
        this.libType = libType;
    }

    public int getIdFromRest() {
        return idFromRest;
    }

    public void setIdFromRest(int id) {
        this.idFromRest = id;
    }

    public Localisation getLocalisation() {
        return localisation;
    }

    public void setLocalisation(Localisation localisation) {
        this.localisation = localisation;
    }

    public Capacite getCapacite() {
        return capacite;
    }

    public void setCapacite(Capacite capacite) {
        this.capacite = capacite;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public boolean getFavori() {
        return favori;
    }

    public void setFavori(boolean favori) {
        this.favori = favori;
    }
}
