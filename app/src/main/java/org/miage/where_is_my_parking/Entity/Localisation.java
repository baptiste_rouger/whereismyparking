package org.miage.where_is_my_parking.Entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Rushnak on 12/03/2018.
 */
@Table(name = "Localisation")
public class Localisation extends Model{

    @Column(name = "Latitude")
    private double latitude;
    @Column(name = "Longitude")
    private double longitude;
    @Column(name = "CodePostal")
    private int codePostal;
    @Column(name = "Commune")
    private String commune;
    @Column(name = "Adresse")
    private String adresse;
    @Column(name = "AccesTransCommuns")
    private String accesTransCommuns;

    public Localisation(){
        super();
    }

    public Localisation(double latitude, double longitude, int codePostal, String commune, String adresse, String accesTransCommuns) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
        this.codePostal = codePostal;
        this.commune = commune;
        this.adresse = adresse;
        this.accesTransCommuns = accesTransCommuns;

    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getAccesTransCommuns() {
        return accesTransCommuns;
    }

    public void setAccesTransCommuns(String accesTransCommuns) {
        this.accesTransCommuns = accesTransCommuns;
    }
}
