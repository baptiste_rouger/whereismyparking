package org.miage.where_is_my_parking.Entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Rushnak on 12/03/2018.
 */
@Table(name = "Capacite")
public class Capacite extends Model {

    @Column(name = "CapaVelo")
    private int capaVelo;
    @Column(name = "CapaVoitures")
    private int capaVoitures;
    @Column(name = "CapaVehiElec")
    private int capaVehiElec;
    @Column(name = "CapaMoto")
    private int capaMoto;
    @Column(name = "CapaPMR")
    private int capaPMR;
    @Column(name = "CapaMax")
    private int capaMax;
    @Column(name = "CurrentCapa")
    private int currentCapa;


    public Capacite() {
        super();
    }

    public Capacite(int capaVelo, int capaVoitures, int capaVehiElec, int capaMoto, int capaPMR) {
        super();
        this.capaVelo = capaVelo;
        this.capaVoitures = capaVoitures;
        this.capaVehiElec = capaVehiElec;
        this.capaMoto = capaMoto;
        this.capaPMR = capaPMR;
        this.currentCapa = capaVelo + capaMoto + capaPMR + capaVoitures + capaVehiElec;
        this.capaMax = capaVelo + capaMoto + capaPMR + capaVoitures + capaVehiElec;

    }

    public int getCapaVelo() {
        return capaVelo;
    }

    public void setCapaVelo(int capaVelo) {
        this.capaVelo = capaVelo;
    }

    public int getCapaVoitures() {
        return capaVoitures;
    }

    public void setCapaVoitures(int capaVoitures) {
        this.capaVoitures = capaVoitures;
    }

    public int getCapaVehiElec() {
        return capaVehiElec;
    }

    public void setCapaVehiElec(int capaVehiElec) {
        this.capaVehiElec = capaVehiElec;
    }

    public int getCapaMoto() {
        return capaMoto;
    }

    public void setCapaMoto(int capaMoto) {
        this.capaMoto = capaMoto;
    }

    public int getCapaPMR() {
        return capaPMR;
    }

    public void setCapaPMR(int capaPMR) {
        this.capaPMR = capaPMR;
    }

    public int getCurrentCapa() {
        return currentCapa;
    }

    public void setCurrentCapa(int currentCapa) {
        this.currentCapa = currentCapa;
    }

    public int getCapaMax() {
        return capaMax;
    }

    public void setCapaMax(int capaMax) {
        this.capaMax = capaMax;
    }
}