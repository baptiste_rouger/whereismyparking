package org.miage.where_is_my_parking.Entity.dao;

import android.util.Log;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import org.miage.where_is_my_parking.Entity.Capacite;
import org.miage.where_is_my_parking.Entity.Parking;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rushnak on 16/04/2018.
 */

public class DaoParking {

    private static List<Parking> listAll = new ArrayList<>();

    public static List<Parking> loadAll(){
        if (listAll.isEmpty()) {
            listAll = new  Select()
                    .from(Parking.class)
                    .execute();
        }
        return listAll;
    }

    public static Parking loadFromRestId(int restId){
        return (Parking) new Select()
                .from(Parking.class)
                .where("IdFromRest = ?", restId)
                .execute().get(0);
    }

    public static List<Parking> searchByAdresse(String adress){
        List<Parking> listByAdress = new ArrayList<>();
        for (Parking p : listAll){
            if (p.getLocalisation().getAdresse().contains(adress)){
                listByAdress.add(p);
            }
        }
        return listByAdress;
    }

    public static List<Parking> searchByFreeSlot(int min){
        List<Parking> listByFreeSlot = new ArrayList<>();
        for (Parking p : listAll){
            if (p.getCapacite().getCurrentCapa() >= min){
                listByFreeSlot.add(p);
            }
        }
        return listByFreeSlot;
    }

    public static List<Parking> searchByName(String name){
        return new Select()
                .from(Parking.class)
                .where("Name LIKE ?", "%"+name+"%")
                .execute();
    }


    public static List<Parking> getFavoris(){
        return new Select()
                .from(Parking.class)
                .where("Favori = 1")
                .execute();
    }

    /*
    True = il faut que le moyen de paiement soit accepté
    False = On en tient pas compte
     */
    public static List<Parking> searchByPayment(Boolean cash, Boolean CB, Boolean total){
        List<Parking> listByPayment = new ArrayList<>();
        for (Parking p : listAll){
            if (!((cash && !p.isCash()) || (CB && !p.isCB()) || (total && !p.isTotalGR()))){
                listByPayment.add(p);
            }
        }
        return listByPayment;
    }

    public static Parking searchByExactName(String name) {
        return (Parking) new Select()
                .from(Parking.class)
                .where("Name = ?", name)
                .execute().get(0);
    }

    public static void updateCapacity(int currentCapa, int restId, int maxCapa){
        try {
            Capacite capa = ((Parking) new Select()
                    .from(Parking.class)
                    .where("IdFromRest = ?", restId)
                    .execute().get(0)).getCapacite();
            capa.setCurrentCapa(currentCapa);
            if (capa.getCapaMax() < maxCapa){
                capa.setCapaMax(maxCapa);
            }
            capa.save();
        }catch(Exception e){
            Log.d("Exception", "Exception lors de la mise à jour de la capacité, un parking présent dans la liste des capacités n'est probablement pas présent dans la liste générale des parkings");
        }
    }

    public static void setFavori(int restId, boolean favOrNot){
        Parking park = ((Parking) new Select()
                .from(Parking.class)
                .where("IdFromRest = ?", restId)
                .execute().get(0));
        park.setFavori(favOrNot);
        park.save();
    }


}
