package org.miage.where_is_my_parking.Entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Rushnak on 12/03/2018.
 */
@Table(name = "Contact")
public class Contact extends Model {

    @Column(name = "SiteWeb")
    private String siteWeb;
    @Column(name = "Exploitant")
    private String exploitant;
    @Column(name = "Tel")
    private String tel;

    public Contact(){
        super();
    }

    public Contact(String siteWeb, String exploitant, String tel) {
        super();
        this.siteWeb = siteWeb;
        this.exploitant = exploitant;
        this.tel = tel;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getExploitant() {
        return exploitant;
    }

    public void setExploitant(String exploitant) {
        this.exploitant = exploitant;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
