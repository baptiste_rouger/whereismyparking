package org.miage.where_is_my_parking.UI.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.miage.where_is_my_parking.Entity.Parking;
import org.miage.where_is_my_parking.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParkingAdapter extends ArrayAdapter<Parking>{
    @BindView(R.id.parking_adapter_name)
    TextView mNameTextView;
    @BindView(R.id.parking_adapter_adress)
    TextView mAdressTextView;
    @BindView(R.id.parking_adapter_place)
    TextView mPlaceTextView;
    @BindView(R.id.parking_adapter_cash)
    ImageView mCashImageView;
    @BindView(R.id.parking_adapter_cb)
    ImageView mCbImageView;
    @BindView(R.id.parking_adapter_total)
    ImageView mTotalImageView;
    @BindView(R.id.parking_adapter_favorite)
    ImageView mFavoriteImageView;


    public ParkingAdapter(Context context, List<Parking> parkings) {
        super(context, -1, parkings);
    }
    @NonNull
    @Override
    public View getView(int i, @Nullable View convertView, @NonNull ViewGroup parent){
        View actualView = convertView;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.parking_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);
        mNameTextView.setText(getItem(i).getName());
        mAdressTextView.setText(getItem(i).getLocalisation().getAdresse());
        if(getItem(i).getCapacite()!=null) {
            String p = String.valueOf(getItem(i).getCapacite().getCurrentCapa()) + "/" + String.valueOf(getItem(i).getCapacite().getCapaMax());
            mPlaceTextView.setText(p);
            if(getItem(i).isCash()){
                mCashImageView.setVisibility(View.VISIBLE);
            }
            if(getItem(i).isCB()){
                mCbImageView.setVisibility(View.VISIBLE);
            }
            if(getItem(i).isTotalGR()){
                mTotalImageView.setVisibility(View.VISIBLE);
            }
            if(getItem(i).getFavori()){
                mFavoriteImageView.setImageResource(R.drawable.ic_favorite_black_24dp);
            }
            else{
                mFavoriteImageView.setVisibility(View.VISIBLE);

            }
        }
        else{
            mPlaceTextView.setVisibility(View.INVISIBLE);
        }

        return actualView;
    }
}
