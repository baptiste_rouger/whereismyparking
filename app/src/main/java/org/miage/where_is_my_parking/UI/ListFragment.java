package org.miage.where_is_my_parking.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import org.miage.where_is_my_parking.UI.Adapter.ParkingAdapter;
import org.miage.where_is_my_parking.Entity.Parking;
import org.miage.where_is_my_parking.Entity.dao.DaoParking;
import org.miage.where_is_my_parking.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class ListFragment extends Fragment {
    @BindView(R.id.parking_list) ListView mListView;
    private ParkingAdapter mParkingAdapter;
    List<Parking> listItems;

    @BindView(R.id.search_adress_list)
    public EditText adressEdit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list,container,false);
        ButterKnife.bind(this, view);
        DaoParking daoParking = new DaoParking();
        listItems = daoParking.loadAll();

        mParkingAdapter = new ParkingAdapter(super.getContext(), listItems);
        mListView.setAdapter(mParkingAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra("id", listItems.get(position).getIdFromRest());
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void refresh(){
        if (mParkingAdapter != null) {
            mParkingAdapter.clear();
            if (adressEdit.getText().toString() != null) {
                mParkingAdapter.addAll(DaoParking.searchByName(adressEdit.getText().toString()));
            } else {
                mParkingAdapter.addAll(DaoParking.loadAll());
            }
        }
    }

    @OnTextChanged(R.id.search_adress_list)
    public void OnTextChanged(CharSequence edit){
        if (!mParkingAdapter.isEmpty()) {
            mParkingAdapter.clear();
        }
        mParkingAdapter.addAll(DaoParking.searchByName(edit.toString()));
    }
}
