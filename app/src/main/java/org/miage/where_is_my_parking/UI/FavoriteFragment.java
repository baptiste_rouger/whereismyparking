package org.miage.where_is_my_parking.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.miage.where_is_my_parking.UI.Adapter.ParkingAdapter;
import org.miage.where_is_my_parking.Entity.Localisation;
import org.miage.where_is_my_parking.Entity.Parking;
import org.miage.where_is_my_parking.Entity.dao.DaoParking;
import org.miage.where_is_my_parking.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FavoriteFragment extends Fragment {
    @BindView(R.id.parking_list) ListView mListView;
    private ParkingAdapter mParkingAdapter;
    List<Parking> listItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list,container,false);
        ButterKnife.bind(this, view);
        DaoParking daoParking = new DaoParking();
        listItems = daoParking.getFavoris();
        if(listItems.isEmpty()){
            Parking p = new Parking();
            p.setName("Aucun favoris");
            p.setLocalisation(new Localisation());
            listItems.add(p);
        }
        mParkingAdapter = new ParkingAdapter(super.getContext(), listItems);
        mListView.setAdapter(mParkingAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra("id", listItems.get(position).getIdFromRest());
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    public void refresh(){
        if (mParkingAdapter != null) {
            mParkingAdapter.clear();
            mParkingAdapter.addAll(DaoParking.getFavoris());
        }
    }
}

