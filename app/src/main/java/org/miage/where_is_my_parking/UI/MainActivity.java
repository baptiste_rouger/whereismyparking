package org.miage.where_is_my_parking.UI;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.squareup.otto.Subscribe;

import org.miage.where_is_my_parking.Entity.Capacite;
import org.miage.where_is_my_parking.Entity.Contact;
import org.miage.where_is_my_parking.Entity.Localisation;
import org.miage.where_is_my_parking.Entity.Parking;
import org.miage.where_is_my_parking.Entity.dao.DaoParking;
import org.miage.where_is_my_parking.EntityREST.Capacities.Capacity;
import org.miage.where_is_my_parking.R;

import org.miage.where_is_my_parking.REST.CurrentCapacitySearchService;
import org.miage.where_is_my_parking.RestToBase.ParkingTransfo;
import org.miage.where_is_my_parking.REST.ParkingSearchService;
import org.miage.where_is_my_parking.event.CapacitySearchResultEvent;
import org.miage.where_is_my_parking.event.EventBusManager;
import org.miage.where_is_my_parking.event.SearchResultEvent;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.util.concurrent.TimeUnit.SECONDS;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private Fragment mFragment;
    public static boolean OffLine= false;
    private ProgressDialog progD;
    private MapsActivity map;
    private ListFragment list;
    private FavoriteFragment fav;
    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    @BindView(R.id.affichage) FrameLayout mFrameLayout;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
      //  private FrameLayout mframe = findViewById(R.id.affichage);

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    setTitle(R.string.title_home);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.affichage, map)
                            .commit();
                    return true;
                case R.id.navigation_dashboard:
                    setTitle(R.string.title_dashboard);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.affichage, list)
                            .commit();
                    return true;
                case R.id.navigation_notifications:
                    setTitle(R.string.title_notifications);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.affichage, fav)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TO-DO réaction en cas de refus des permissions également demander l'activation du GPS
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, 123);
        }

        Configuration.Builder config = new Configuration.Builder(this);
        config.addModelClasses(Capacite.class, Contact.class, Localisation.class, Parking.class);
        ActiveAndroid.initialize(config.create());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        map = new MapsActivity();
        list = new ListFragment();
        fav = new FavoriteFragment();
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (isNetworkConnected()) {
            //Fonctionnement semble hasardeux, à surveiller
            progD = ProgressDialog.show(this, "Chargement", "Chargement des parkings en cours");

            //Vérification du temps écoulé depuis la dernière requête à la liste des parkings
            Date currentDate=new Date();
            SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
            long millis = sharedPreferences.getLong("DATE_UPDATE_PARKING", 0L);
            Date theDate = new Date(millis);

            //Si la dernière requête date de plus d'un mois, ou si l'on a jamais récupéré les parkings, on récupère les parkings
            if (DaoParking.loadAll().isEmpty() || theDate.before(new Date(currentDate.getTime() - (86400000L * 30)))) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("DATE_UPDATE_PARKING", currentDate.getTime());
                editor.commit();
                Log.d("Update", "Update Database");
                ParkingSearchService.INSTANCE.searchParkings();
            } else {
                CurrentCapacitySearchService.INSTANCE.SearchCapacity();
            }

            // Refresh des capacités automatique toutes les 10 minutes
            final Runnable updater = new Runnable() {
                public void run() {  CurrentCapacitySearchService.INSTANCE.SearchCapacity(); }
            };
            scheduler.scheduleAtFixedRate(updater, 600, 600, SECONDS);

        }else {
            if(DaoParking.loadAll().isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Impossible de récupérer les données nécessaires au bon fonctionnement, veuillez activer votre connexion Internet lors de la première utilisation")
                        .setTitle("Récupération des données Impossibles");
                AlertDialog dialog = builder.create();
                dialog.show();
                OffLine = true;
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Impossible de se connecter à Internet, mode hors ligne activé, nombre de places libres indisponibles")
                        .setTitle("Mode hors ligne");
                AlertDialog dialog = builder.create();
                dialog.show();
                OffLine = true;
            }
        }
        //------------------------------------------------------------------
        if (findViewById(R.id.affichage) != null) {
            if (savedInstanceState != null) {
                return;
            }
            android.support.v4.app.Fragment firstFragment = new android.support.v4.app.Fragment();
            firstFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.affichage, map)
                    .commit();
            setTitle(R.string.title_home);

        }
        //------------------------------------------------------------------
    }

    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        // Do NOT forget to call super.onPause()
        super.onPause();
    }

    @Subscribe
    public void searchResultParkings(final SearchResultEvent event) {
        if (DaoParking.loadAll().isEmpty()) {
            ParkingTransfo.INSTANCE.build(event.getParkings());
        } else {
            ParkingTransfo.INSTANCE.update(event.getParkings());
        }
        CurrentCapacitySearchService.INSTANCE.SearchCapacity();


    }

    @Subscribe
    public void searchResultCapacities(final CapacitySearchResultEvent event) {
        for (Capacity c : event.getCapacities()) {
            DaoParking.updateCapacity(c.currentCapa, c.idFromRest, c.maxCapa);
        }
        Log.v("Update", "Capacités mise à jour");
        if (progD.isShowing()) {
            progD.hide();
        }
        // Au lieu de récupérer la map pour s'assurer d'avoir les marqueurs, on aurait pu abonner la MapsActivity à l'event et créer les parkings dans la méthode @subscribe mais on aurait eu des parkings non traités.
        map.createMarkers();
        list.refresh();
        fav.refresh();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite :
                CurrentCapacitySearchService.INSTANCE.SearchCapacity();
                progD = ProgressDialog.show(this, "Chargement", "Chargement des places disponibles en cours en cours");
                break;
        }

        return true;
    }
}
