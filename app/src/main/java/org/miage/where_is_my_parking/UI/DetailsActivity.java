package org.miage.where_is_my_parking.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import org.miage.where_is_my_parking.UI.Adapter.ParkingAdapter;
import org.miage.where_is_my_parking.Entity.Capacite;
import org.miage.where_is_my_parking.Entity.Contact;
import org.miage.where_is_my_parking.Entity.Localisation;
import org.miage.where_is_my_parking.Entity.Parking;
import org.miage.where_is_my_parking.Entity.dao.DaoParking;
import org.miage.where_is_my_parking.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity
        implements OnStreetViewPanoramaReadyCallback {

    private int idParking;
    private Parking parking;
    private Localisation localisation;
    private Capacite capacite;
    private Contact contact;
    private Spanned link;
    @BindView(R.id.parking_detail_name)
    TextView mTextViewName;
    @BindView(R.id.parking_detail_city)
    TextView mTextViewCity;
    @BindView(R.id.parking_detail_adress)
    TextView mTextViewAdress;
    @BindView(R.id.parking_detail_trans)
    TextView mTextViewTrans;
    @BindView(R.id.parking_detail_dispo)
    TextView mTextViewDispo;
    @BindView(R.id.parking_detail_pmr)
    TextView mTextViewPmr;
    @BindView(R.id.parking_detail_car)
    TextView mTextViewCar;
    @BindView(R.id.parking_detail_ecar)
    TextView mTextViewEcar;
    @BindView(R.id.parking_detail_moto)
    TextView mTextViewMoto;
    @BindView(R.id.parking_detail_bike)
    TextView mTextViewBike;
    @BindView(R.id.parking_detail_ex)
    TextView mTextViewEx;
    @BindView(R.id.parking_detail_web)
    TextView mTextViewWeb;
    @BindView(R.id.parking_detail_phone)
    TextView mTextViewPhone;
    @BindView(R.id.parking_detail_desc)
    TextView mTextViewDesc;
    @BindView(R.id.parking_detail_fav)
    ImageView nImageViewFav;
    @BindView(R.id.parking_detail_bp)
    ImageView nImageViewBp;
    @BindView(R.id.parking_detail_bps)
    ImageView nImageViewBps;
    @BindView(R.id.parking_detail_pmrAcces)
    ImageView nImageViewPmrAcces;
    @BindView(R.id.parking_detail_cash)
    ImageView nImageViewCash;
    @BindView(R.id.parking_detail_cb)
    ImageView nImageViewCb;
    @BindView(R.id.parking_detail_total)
    ImageView nImageViewTotal;
    private ParkingAdapter mParkingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        idParking = intent.getIntExtra("id", -1);
        System.out.println(idParking);
        if (idParking == -1){
            Log.v("Exception", "Mauvais id passé à l'activité détail");
        }
        parking = DaoParking.loadFromRestId(idParking);
        localisation = parking.getLocalisation();
        capacite = parking.getCapacite();
        contact = parking.getContact();
        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);

        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
        mTextViewName.setText(parking.getName());
        mTextViewCity.setText(localisation.getCodePostal()+" "+localisation.getCommune());
        mTextViewAdress.setText(localisation.getAdresse());
        mTextViewDispo.setText(capacite.getCurrentCapa()+"/"+capacite.getCapaMax());
        mTextViewPmr.setText(String.valueOf(capacite.getCurrentCapa()));
        mTextViewCar.setText(String.valueOf(capacite.getCapaVelo()));
        mTextViewEcar.setText(String.valueOf(capacite.getCapaVehiElec()));
        mTextViewMoto.setText(String.valueOf(capacite.getCapaMoto()));
        mTextViewBike.setText(String.valueOf(capacite.getCapaVelo()));
        mTextViewEx.setText(contact.getExploitant());
        link =  Html.fromHtml("<a href=\""+contact.getSiteWeb()+"\">"+contact.getSiteWeb()+"</a>");
        mTextViewWeb.setMovementMethod(LinkMovementMethod.getInstance());
        mTextViewWeb.setText(link);
        mTextViewPhone.setText(contact.getTel());
        Linkify.addLinks(mTextViewPhone, Linkify.ALL);
        mTextViewDesc.setText(parking.getPresentation());
        mTextViewTrans.setText("Transport en commun : "+localisation.getAccesTransCommuns());
        if(parking.isCash()){
            nImageViewCash.setVisibility(View.VISIBLE);
        }
        if(parking.isCB()){
            nImageViewCb.setVisibility(View.VISIBLE);
        }
        if(parking.isTotalGR()){
            nImageViewTotal.setVisibility(View.VISIBLE);
        }
        if(parking.isStationnementVelo()){
            nImageViewBp.setVisibility(View.VISIBLE);
        }
        if(parking.isStationnementVeloSecurise()){
            nImageViewBps.setVisibility(View.VISIBLE);
        }
        if(parking.isAccesPMR()){
            nImageViewPmrAcces.setVisibility(View.VISIBLE);
        }
        if(parking.getFavori()){
            nImageViewFav.setImageResource(R.drawable.ic_favorite_black_24dp);
        }
        nImageViewFav.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               if(parking.getFavori()) {
                   parking.setFavori(false);
                   nImageViewFav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                   parking.save();
                   Toast.makeText(DetailsActivity.this, "Retiré des favoris", Toast.LENGTH_LONG).show();
               }
                else{
                   parking.setFavori(true);
                   nImageViewFav.setImageResource(R.drawable.ic_favorite_black_24dp);
                   parking.save();
                   Toast.makeText(DetailsActivity.this, "Ajouté aux favoris", Toast.LENGTH_LONG).show();
               }


            }
        });
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        streetViewPanorama.setPosition(new LatLng(parking.getLocalisation().getLatitude(), parking.getLocalisation().getLongitude()));
    }
}
