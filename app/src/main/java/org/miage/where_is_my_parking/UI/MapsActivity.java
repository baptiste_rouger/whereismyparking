package org.miage.where_is_my_parking.UI;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.miage.where_is_my_parking.Entity.Parking;
import org.miage.where_is_my_parking.Entity.dao.DaoParking;
import org.miage.where_is_my_parking.R;
import org.miage.where_is_my_parking.REST.CurrentCapacitySearchService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;


public class MapsActivity extends Fragment implements OnMapReadyCallback, LocationSource.OnLocationChangedListener {

    MapView mMapView;
    private GoogleMap googleMap;
    private Marker locationMarker;
    private List<Marker> parkingMarkers;
    private static FusedLocationProviderClient mFusedLocationClient;
    public static Location currentLocation;
    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    @BindView(R.id.activity_main_search_adress_edittext)
    public EditText adressEdit;

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_maps, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        parkingMarkers = new ArrayList<>();

        ButterKnife.bind(this, rootView);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.getActivity());
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this.getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            currentLocation = location;
                            if (googleMap != null) {
                                locationMarker = googleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Votre position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
                                // For zooming automatically to the location of the marker
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(12).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                        }
                    }
                });


        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                createMarkers();
                mFusedLocationClient.getLastLocation();
                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent i = new Intent(MapsActivity.this.getContext(), DetailsActivity.class);
                        i.putExtra("id", DaoParking.searchByExactName(marker.getTitle()).getIdFromRest());
                        MapsActivity.this.startActivity(i);
                    }
                });
            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }


    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        if (locationMarker != null){
            locationMarker.remove();
        }
        locationMarker = googleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Votre position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
    }

    public void createMarkers(){
        //Si la map n'a pas encore été instancié
        if (googleMap == null){
            final Runnable creator = new Runnable() {
                public void run() {  createMarkers(); }
            };
            scheduler.schedule(creator, 500, MILLISECONDS);
        }else {
            if (!parkingMarkers.isEmpty()) {
                for (Marker m : parkingMarkers){
                    m.remove();
                }
                parkingMarkers.clear();
            }
            if (adressEdit.getText().toString() != null){
                for (Parking p : DaoParking.searchByName(adressEdit.getText().toString())) {
                    LatLng parking = new LatLng(p.getLocalisation().getLatitude(), p.getLocalisation().getLongitude());
                    Marker marker = googleMap.addMarker(new MarkerOptions().position(parking).title(p.getName()).snippet(p.getLocalisation().getAdresse() + ", "  + p.getCapacite().getCurrentCapa() + " places dispos"));
                    parkingMarkers.add(marker);
                }
            } else {
                for (Parking p : DaoParking.loadAll()) {
                    LatLng parking = new LatLng(p.getLocalisation().getLatitude(), p.getLocalisation().getLongitude());
                    Marker marker = googleMap.addMarker(new MarkerOptions().position(parking).title(p.getName()).snippet(p.getLocalisation().getAdresse() + ", " + p.getCapacite().getCurrentCapa() + " places dispos"));
                    parkingMarkers.add(marker);
                }
            }
        }
    }

    @OnTextChanged(R.id.activity_main_search_adress_edittext)
    public void OnTextChanged(CharSequence edit){
        if (!parkingMarkers.isEmpty()) {
            for (Marker m : parkingMarkers){
                m.remove();
            }
            parkingMarkers.clear();
        }
        for (Parking p : DaoParking.searchByName(edit.toString())) {
            LatLng parking = new LatLng(p.getLocalisation().getLatitude(), p.getLocalisation().getLongitude());
            Marker marker = googleMap.addMarker(new MarkerOptions().position(parking).title(p.getName()).snippet(p.getLocalisation().getAdresse() + ", "  + p.getCapacite().getCurrentCapa() + " places dispos"));
            parkingMarkers.add(marker);
        }
    }

}