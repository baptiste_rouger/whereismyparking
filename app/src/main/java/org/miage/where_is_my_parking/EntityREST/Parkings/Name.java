package org.miage.where_is_my_parking.EntityREST.Parkings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rushnak on 09/02/2018.
 */

public class Name {
    @SerializedName("name")
    @Expose
    public String name;
}

