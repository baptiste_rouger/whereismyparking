package org.miage.where_is_my_parking.EntityREST.Parkings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rushnak on 09/02/2018.
 */

public class Parkings {
    @SerializedName("data")
    @Expose
    public List<Parking> parkingList;
}
