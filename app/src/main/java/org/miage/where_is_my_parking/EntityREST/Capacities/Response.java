package org.miage.where_is_my_parking.EntityREST.Capacities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rushnak on 16/04/2018.
 */

public class Response {
    @SerializedName("opendata")
    @Expose
    public OpenData openData;
}
