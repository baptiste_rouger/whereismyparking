package org.miage.where_is_my_parking.EntityREST.Parkings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rushnak on 09/02/2018.
 */

public class Parking {

    @SerializedName("geo")
    @Expose
    public Name name;

    @SerializedName("_l")
    @Expose
    public double[] latLong;

    @SerializedName("SERVICE_VELO")
    @Expose
    public String serviceVelo;

    @SerializedName("CAPACITE_VOITURE")
    @Expose
    public int capaVoitures;

    @SerializedName("INFOS_COMPLEMENTAIRES")
    @Expose
    public String infoComp;

    @SerializedName("STATIONNEMENT_VELO")
    @Expose
    public String stationnementVelo;

    @SerializedName("STATIONNEMENT_VELO_SECURISE")
    @Expose
    public String stationnementVeloSecurise;

    @SerializedName("CAPACITE_VEHICULE_ELECTRIQUE")
    @Expose
    public int capaVehiElec;

    @SerializedName("CODE_POSTAL")
    @Expose
    public int codePostal;

    @SerializedName("ACCES_PMR")
    @Expose
    public String accesPMR;

    @SerializedName("TELEPHONE")
    @Expose
    public String tel;

    @SerializedName("MOYEN_PAIEMENT")
    @Expose
    public String moyenPaiement;

    @SerializedName("ACCES_TRANSPORTS_COMMUNS")
    @Expose
    public String accesTransCommuns;

    @SerializedName("CAPACITE_MOTO")
    @Expose
    public int capaMoto;

    @SerializedName("CONDITIONS_D_ACCES")
    @Expose
    public String condAcces;

    @SerializedName("COMMUNE")
    @Expose
    public String commune;

    @SerializedName("CAPACITE_VELO")
    @Expose
    public int capaVelo;

    @SerializedName("PRESENTATION")
    @Expose
    public String presentation;

    @SerializedName("CAPACITE_PMR")
    @Expose
    public int capaPMR;

    @SerializedName("LIBCATEGORIE")
    @Expose
    public String libCategorie;

    @SerializedName("EXPLOITANT")
    @Expose
    public String exploitant;

    @SerializedName("SITE_WEB")
    @Expose
    public String siteWeb;

    @SerializedName("ADRESSE")
    @Expose
    public String adresse;

    @SerializedName("LIBTYPE")
    @Expose
    public String libType;

    @SerializedName("_IDOBJ")
    @Expose
    public int id;

    public void setDefault() {
        if (name.name == null)
            name.name = "";
        if (serviceVelo == null)
            serviceVelo = "";
        if (infoComp == null)
            infoComp = "";
        if (stationnementVelo == null)
            stationnementVelo = "";
        if (stationnementVeloSecurise == null)
            stationnementVeloSecurise = "";
        if (accesPMR == null)
            accesPMR = "";
        if (tel == null)
            tel = "";
        if (moyenPaiement == null)
            moyenPaiement = "";
        if (accesTransCommuns == null)
            accesTransCommuns = "";
        if (condAcces == null)
            condAcces = "";
        if (commune == null)
            commune = "";
        if (presentation == null)
            presentation = "";
        if (libCategorie == null)
            libCategorie = "";
        if (exploitant == null)
            exploitant = "";
        if (siteWeb == null)
            siteWeb = "";
        if (adresse == null)
            adresse = "";
        if (libType == null)
            libType = "";

    }
}
