package org.miage.where_is_my_parking.EntityREST.Capacities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rushnak on 16/04/2018.
 */

public class GrpParkings {

    @SerializedName("Groupe_Parking")
    @Expose
    public List<Capacity> capacities;
}
