package org.miage.where_is_my_parking.EntityREST.Capacities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.miage.where_is_my_parking.EntityREST.Parkings.Name;

/**
 * Created by Rushnak on 16/04/2018.
 */

public class Capacity {

    @SerializedName("Grp_disponible")
    @Expose
    public int currentCapa;

    @SerializedName("IdObj")
    @Expose
    public int idFromRest;

    @SerializedName("Grp_exploitation")
    @Expose
    public int maxCapa;
}
